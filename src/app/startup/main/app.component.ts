import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../../services/auth.service';
import { User } from '../../models/user.model';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})

export class AppComponent {
	currentUser: User;
	title = 'Demo App Address';

	constructor(
		private router: Router,
		private authenticationService: AuthenticationService
	) {
		this.authenticationService.currentUser.subscribe(
			x => (this.currentUser = x)
		);
	}

	decode(str: string) {
		console.log(atob(str));
		return atob(str);
	}

	logout() {
		this.authenticationService.logout();
		this.router.navigate(['/login']);
	}
}
