import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './main/app.component';
import { EditComponent } from '../components/edit/edit.component';
import { ViewComponent } from '../components/view/view.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from '../helpers/auth.guard';

const routes: Routes = [
	{
		path: 'login',
		component: LoginComponent
	},
	{
		path: 'edit',
		component: EditComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'view',
		component: ViewComponent,
		// canActivate: [AuthGuard]
	},
	{
		path: '**',
		redirectTo: '/view',
		pathMatch: 'full'
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {}
