import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from '../startup/app-routing.module';
import { JwtInterceptor } from '../interceptors/token.interceptor';
import { ErrorInterceptor } from '../interceptors/http-error.interceptor';
import { AppComponent } from './main/app.component';

import { AlertComponent } from '../components/alert/alert.component';
import { EditComponent } from '../components/edit/edit.component';
import { ViewComponent } from '../components/view/view.component';
import { LoginComponent } from './login/login.component';

@NgModule({
	declarations: [
		AppComponent,
		EditComponent,
		LoginComponent,
		ViewComponent
	],
	imports: [
		BrowserModule,
		ReactiveFormsModule,
		HttpClientModule,
		AppRoutingModule
	],
	providers: [
		{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
		{ provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
		AlertComponent
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
