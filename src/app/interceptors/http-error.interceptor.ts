import { Injectable } from '@angular/core';
import {
	HttpRequest,
	HttpHandler,
	HttpEvent,
	HttpInterceptor,
	HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthenticationService } from '../services/auth.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
	constructor(private authenticationService: AuthenticationService) {}

	intercept(
		req: HttpRequest<any>,
		next: HttpHandler
	): Observable<HttpEvent<any>> {
		return next.handle(req).pipe(
			catchError(err => {
				console.log(req);
				if (err.status === 401) {
					// auto logout if 401 response returned from api
					this.authenticationService.logout();
					location.reload();
				}
				const error: HttpErrorResponse = err.error.message || err.statusText || err.error || err;
				// console.log(error);
				return throwError(error);
			})
		);
	}
}
